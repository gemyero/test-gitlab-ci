const express = require('express');

const app = express();
const { PORT: port = 3000 } = process.env;

app.get('/', (req, res) => {
  res.send('Test Gitlab CI/CD');
});

app.listen(port, () => console.log(`Server listens on port ${port}`));